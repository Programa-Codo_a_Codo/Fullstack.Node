const express = require("express");
const router = express.Router();

let usuarios = [
  {
    id: 1,
    nombre: "Marcos",
    apellido: "Sosa",
    email: "marcos.sosa@gmail.com",
  },
  {
    id: 2,
    nombre: "Javier",
    apellido: "Ramos",
    email: "javier.ramos@gmail.com",
  },
  {
    id: 3,
    nombre: "Diego",
    apellido: "Maradona",
    email: "diego.maradona@gmail.com",
  },
];

//GET ALL
router.get("/", (req, res) => {
  res.json(usuarios);
});

// GET by ID
router.get("/:id", (req, res) => {
  const user = usuarios.find((u) => u.id === parseInt(req.params.id));

  if (!user) return res.status(404).send("Usuario NO encontrado");

  res.json(user);
});

// POST
router.post("/", (req, res) => {
  const nuevoUsuario = {
    id: usuarios.length + 1,
    nombre: req.body.nombre,
    apellido: req.body.apellido,
    email: req.body.email,
  };

  usuarios.push(nuevoUsuario);

  res.status(201).json(nuevoUsuario);
});

// PUT by ID
router.put("/:id", (req, res) => {
  const user = usuarios.find((u) => u.id === parseInt(req.params.id));

  // Editar algunas de las caracteristicas del usuario encontrado

  user.nombre = req.body.nombre || user.nombre;
  user.apellido = req.body.apellido || user.apellido;
  user.email = req.body.email || user.email;

  res.json(user);
});

// DELETE by ID
router.delete("/:id", (req, res) => {
  const indexUser = usuarios.findIndex((u) => u.id === parseInt(req.params.id));

  if (indexUser === -1)
    return res.status(404).send("Usuario con el indice NO ENCONTRADO");

  const usuarioEliminado = usuarios.splice(indexUser, 1);

  res.json(usuarioEliminado);
});

module.exports = router;
