function validateForm(event) {
  event.preventDefault();

  //inputs
  let username = document.getElementById("username").value;
  let email = document.getElementById("email").value;

  if (username.trim() === "") {
    alert("por favor ingresa un usuario");
    return false; // EVITA QUE SE ENVIE EL FORMULARIo
  }

  if (email.trim() === "") {
    alert("por favor ingresa un mail");
    return false;
  }

  if (!isValidEmail(email)) {
    alert("Por favor ingresa un MAIL VALIDO");
    return true;
  }

  function isValidEmail(email) {
    let emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    return emailRegex.test(email);
  }

  alert("Formulario enviado correctamente");
  return true;
}

document.getElementById("myForm").addEventListener("submit", validateForm);
