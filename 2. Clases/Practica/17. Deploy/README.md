## **Deploy en Vercel**

1. **Instalamos dotenv**

```js
npm install dotenv
```

y en el archivo de configuracion de conexion con la base de datos, hacemos uso de la dependencia.

![IMAGEN_1](./img/1.png)

por otro lado creamos un archivo `.env` que contendra nuestras variables de configuración.

![IMAGEN_2](./img/2.png)

```.env
DB_HOST = "localhost"
DB_USER = "root"
DB_PASSWORD = "root"
DB_NAME = "posystem"
```

2. **Creamos un archivo `vercel.json` con la configuracion que necesita vercel.**

```json
{
    "version": 2,
    "builds": [
        {"src": "index.js", "use": "@vercel/node" }
    ],
    "routes": [
        { "src": "/(.*)", "dest": "index.js" }
    ]
}
```

![IMAGEN_3](./img/3.png)

3. **Subimos a GitHub o GitLab el codigo con todas estas nuevas configuraciones.**

- Observacion:
  - No es necesario subir la carpeta `node_modules` ya que eso vercel lo genera cuando haga el deploy.
  - El archivo `.env` puede subirse con datos falsos por primera vez, y luego crear un `.gitignore` para luego ignorar los datos reales.

```.gitignore
# dependencies
/node_modules

# config db
.env
```

![IMAGEN_4](./img/4.png)

![IMAGEN_5](./img/5.png)

4. **Creamos nuestra bd y estructura (ejecutamos el script de inicializacion de la bd) en un servidor gratuito o pago. Por ej https://www.freesqldatabase.com/. Obtenemos los datos de conexion con la bd y las guardamos para configurar vercel.**

![IMAGEN_6](./img/6.png)

5. **En Vercel, importamos de Git el proyecto, y en la seccion `Environment Variables`, pegamos las variables de entorno con el tag (1° imagen) y los datos correspondientes (6° imagen).**

![IMAGEN_7](./img/7.png)

- Observación: Cuando hacemos esto, si tenemos subido el `.env` con los datos de conexion local. Estos son pisados y tienen prioridad los datos de conexion de vercel. Por lo tanto independientemente que este o no el `.env` la conexion se hara con la base de datos remota.

Finalmente hacemos click en `Deploy`.
