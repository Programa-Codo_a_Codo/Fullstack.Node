DROP DATABASE IF EXISTS usuarios_db;

CREATE DATABASE usuarios_db;

USE usuarios_db;

CREATE TABLE usuarios (
	id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    apellido VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    ruta_archivo VARCHAR(255)
);

INSERT INTO usuarios (nombre, apellido, email) VALUES
("Juan", "Perez", "juan.perez@gmail.com"),
("Pedro", "Ramos", "pedro.ramos@gmail.com"),
("Martin", "Martinez", "martin.martinez@gmail.com");

SELECT * FROM usuarios;