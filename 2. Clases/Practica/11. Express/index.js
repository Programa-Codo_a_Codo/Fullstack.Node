// ----- SERVIDOR ESTATICO NATIVO CON EXPRESS -----

const express = require("express");

const app = express();
let port = 3000;

//Middleware
app.use(express.static("public"));

/*
app.get("/", (req, res) => {
  // enviar solo un mensaje (texto plano)
  // res.send("HOLA DESDE UN SERVIDOR EXPRESS");

  // enviar un archivo
  // res.sendFile(__dirname + '/public/index.html');
});
*/

app.get("/hola", (req, res) => {
  // enviar solo un mensaje (texto plano)
  console.log("----------------------");
  console.log(req);
  res.send("Estamos en la ruta /hola");
});

app.get("/contacto", (req, res) => {
  res.sendFile(__dirname + "/public/contacto.html");
});

app.post("/contacto", (req, res) => {
  res.send("DESDE PAGINA DE CONTACTO SE HA ENVIADO ALGO");
});

app.listen(port, () => {
  console.log(`Servidor ejecutandose en el pueto ${port}`);
});
