//EVENT LISTENER (TIPO DE ENVENTO , CALLBACK )

// Crea el elemento
let btn = document.createElement("button");
// Crea el id
btn.id = "toggleNoche";
// Crea el boton
btn.textContent = "Cambiar a modo Noche";
document.body.appendChild(btn);
// Cambia el modo de noche
document.addEventListener("DOMContentLoaded", function () {
  // Obtiene el boton
  let btnModoNoche = document.getElementById("toggleNoche");
  // Obtiene el body
  let body = document.body;
  // Funcion para activar el modo noche
  function ActivarModoNoche() {
    // Cambia/Switchea el color del body
    body.classList.toggle("modo-noche");
    // Si el modo es noche cambia el texto del boton
    if (body.classList.contains("modo-noche")) {
      // Cambia el texto del boton, modo dia
      btnModoNoche.innerHTML = "ACTIVAR MODO DIA";
    } else {
      // Cambia el texto del boton, modo noche
      btnModoNoche.innerHTML = "ACTIVAR MODO NOCHE";
    }
  }
  // Al hacer click en el boton activa el modo noche
  btnModoNoche.addEventListener("click", function () {
    // Llama a la funcion
    ActivarModoNoche();
  });
});
