const express = require("express");
const router = express.Router();
const userController = require("../controller/userController");


router.get("/", userController.ObtenerTodosLosUsuarios);
router.get("/:id", userController.ObtenerUsuariosPorId);
router.post("/", userController.CrearUsuario);
router.put("/:id", userController.ActualizarUsuario);
router.delete("/:id", userController.BorrarUsuario);


module.exports = router;
