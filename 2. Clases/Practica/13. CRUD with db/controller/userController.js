const db = require("../db/db");

const ObtenerTodosLosUsuarios = (req, res) => {
  const sql = "SELECT * FROM usuarios";

  db.query(sql, (err, result) => {
    if (err) throw err;

    res.json(result);
  });
};

const ObtenerUsuariosPorId = (req, res) => {
  const { id } = req.params;
  const sql = "SELECT * FROM usuarios WHERE id = ?";

  db.query(sql, [id], (err, result) => {
    if (err) throw err;
    res.json(result);
  });
};

const CrearUsuario = (req, res) => {
  const { nombre, apellido, email } = req.body;

  const sql =
    "INSERT INTO usuarios ( nombre, apellido, email ) VALUES ( ?, ?, ? )";

  db.query(sql, [nombre, apellido, email], (err, result) => {
    if (err) throw err;

    res.json({
      mensaje: "Usuario Creado con EXITO",
      idUsuario: result.insertId,
    });
  });
};

const ActualizarUsuario = (req, res) => {
  const { id } = req.params;
  const { nombre, apellido, email } = req.body;

  const sql =
    "UPDATE usuarios SET nombre = ?, apellido = ? , email = ? WHERE id = ?";

  db.query(sql, [nombre, apellido, email, id], (err, result) => {
    if (err) throw err;

    res.json({
      mensaje: "Usuario EDITADO",
    });
  });
};

const BorrarUsuario = (req, res) => {
  const { id } = req.params;

  const sql = "DELETE FROM usuarios WHERE id = ?";

  db.query(sql, [id], (err, result) => {
    if (err) throw err;

    res.json({
      mensaje: "usuario ELIMINADO con EXITO",
    });
  });
};

module.exports = {
  ObtenerTodosLosUsuarios,
  ObtenerUsuariosPorId,
  CrearUsuario,
  ActualizarUsuario,
  BorrarUsuario,
};
