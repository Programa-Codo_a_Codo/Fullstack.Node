const mySql = require("mysql");

const connection = mySql.createConnection({
  host: "127.0.0.1",
  port: "3306",
  user: "root",
  password: "root",
  database: "usuarios_db",
});

connection.connect((err) => {
  if (err) {
    console.error("ERROR conectando a la base de datos", err);
    return;
  }
  console.log("Conectado EXITOSAMENTE a la base de datos");

  // Creacion de la base de datos y tabla si no existe.
  connection.query(
    "CREATE DATABASE IF NOT EXISTS usuarios_db",
    (err, results) => {
      if (err) {
        console.log("Error creando la base de datos");
        return;
      }

      console.log("Base de datos asegurada");

      connection.changeUser({ database: "usuarios_db" }, (err) => {
        if (err) {
          console.error("Error al cambiar a usuarios_db", err);
          return;
        }

        const createTableQuery = `
          CREATE TABLE IF NOT EXISTS usuarios (
            id INT AUTO_INCREMENT PRIMARY KEY,
            nombre VARCHAR(100) NOT NULL,
            apellido VARCHAR(100) NOT NULL,
            email VARCHAR(100) NOT NULL
          );            
        `;

        connection.query(createTableQuery, (err, results) => {
          if (err) {
            console.log("Error creando la tabla: ", err);
            return;
          }

          console.log("Tabla asegurada");
        });
      });
    }
  );
});

module.exports = connection;
