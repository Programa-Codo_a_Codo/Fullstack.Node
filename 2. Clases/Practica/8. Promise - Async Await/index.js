/* ************************************************************************* */

// Set Time Out
// retrasa la ejecucion de nuestro programa

/*
console.log("Inicio");

setTimeout(() => {
  console.log("Trabajando durante 4 segundos");
}, 4000);

let mensaje = () => {
  console.log("Trabajando durante 2 segundos");
};
setTimeout(mensaje, 2000);

console.log("Fin");
*/
/* ************************************************************************* */

// Los tres procesos corren al mismo tiempo, y todos los procesos van a terminar a los 5 segundos.
// mientras que si fueran procesos sincronos, entonces tardarian 9 segundos en total.

/*
let mensajeUno = () => {
  setTimeout(() => {
    return console.log("Hola desde el mensaje 1");
  }, 3000);
};
let mensajeDos = () => {
  setTimeout(() => {
    return console.log("Hola desde el mensaje 2");
  }, 5000);
};

let mensajeTres = () => {
  setTimeout(() => {
    return console.log("Hola desde el mensaje 3");
  }, 1000);
};

mensajeUno();
mensajeDos();
mensajeTres();
*/
/* ************************************************************************* */
/*
PROMESAS

Estados de las promesas

- Pending (pendiente) incierto indefinidamente.
- Fullfilled (cumplida) la promesa se cumple y por ende devuelve el resultado que se esta pidiendo.
- Rejected (rechazada) esto devuelve un error.

Las promesas tiene tres metodos con las que podemos trabajar. 
.then
.catch
.finally
*/
/*
function Esperar() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("Hello world");
    }, 3000);
  });
}

console.log("Inicio");

//then se ejecuta si la promesa se resuelve con exito.
//catch se ejecuta si la promesa devuelve error. Es recomendado.
//finally se ejecuta independientemente si la promesa se resuelve o devuelve un error. Es opcional.
Esperar()
  .then((valor) => {
    console.log(valor);
  })
  .catch((error) => {
    console.log("Ha j¿habido un error en el programa" + error);
  })
  .finally(() => {
    console.log("Fin del proceso");
  });

console.log("Fin");
*/
/* ************************************************************************* */
/*
ASYNC AWAIT
*/

/*
function SimularTiempo(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function ejemploAsincrono() {
  console.log("Simulando a que el proceso me devuelva un resultado");
  await SimularTiempo(5000);
  console.log("Espera terminada ya obtuve mis datos");
  await SimularTiempo(3000);
  console.log("Estamos haciendo otra cosa");
  await SimularTiempo(4000);
  console.log("Proceso terminado");
}

ejemploAsincrono();
console.log("Los tres procesos se esperan ya que una es dependiente (en teoria) de la otra. Aqui seguimos en nuestro programa.");
*/

/*
function SimularTiempo(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function ObtenerDatos() {
  await SimularTiempo(4000);

  const error = true;
  if (error) {
    throw new Error(
      "ERROR! NO SE PUDIERON OBTENER LOS DATOS.. POR ALGUN MOTIVO."
    );
  }

  return "datos obtenidos";
}

async function MostrarDatos() {
  try {
    let datos = await ObtenerDatos();
    console.log(datos);
  } catch (error) {
    console.log("Tuvimos el siguiente error: " + error);
  }
}

MostrarDatos();
*/