// ----- SERVIDOR ESTATICO NATIVO CON NODE JS -----

const http = require("http");
const fs = require("fs");

let puerto = 8080;

// CALLBACK (REQ, RES)

const server = http.createServer((req, res) => {
  const url = req.url;

  if (url === "/") {
    const archivo = fs.readFileSync(__dirname + "/index.html");

    res.writeHead(200, { "Content-Type": "text/html" });

    res.end(archivo);
  } else if (url === "/contacto") {
    res.writeHead(200, { "Content-Type": "text/plain" });

    res.end("CONTACTO");
  }
});

server.listen(puerto, () => {
  console.log(`Servidor ARRIBA escuchando en el puerto ${puerto}`);
});
