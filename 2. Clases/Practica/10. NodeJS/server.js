// ----- SERVIDOR ESTATICO NATIVO CON NODE JS -----

const http = require("http");
let puerto = 3000;

// CALLBACK (REQ, RES)

const server = http.createServer((req, res) => {
  res.writeHead(200, { "Content-Type": "text/html" });
  res.end(`
    <h1>BIENVENIDOS A MI PAGINA WEB</h1>
  `);
});

server.listen(puerto, () => {
  console.log(`Servidor ARRIBA escuchando en el puerto ${puerto}`);
});
