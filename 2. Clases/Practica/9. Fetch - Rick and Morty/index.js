/*
AJAX

AJAX solicita informacion que luego es añadida a nuestra pagina sin recargar. Nos permite modificar las porciones
de un sitio.
Nos va a permitir que nunca recarguemos una pagina.
AJAX : Asyncronous Javascript and XML

FETCH

Forma parte del lenguaje nativo de JavaScript y lo que puedo hacer con el es traerme informacion. Por ejemplo de 
una API Restfull.
*/

let main_container = document.getElementById("characters");

let getCharacters = async () => {
  let response = await fetch("https://rickandmortyapi.com/api/character");

  let data = await response.json();

  let { info, results } = data; //DESTRUCTURING

  results.forEach((character) => {
    let articulo = document.createElement("article");
    articulo.className = "articulo";
    articulo.innerHTML = `
      <img src="${character.image}">
      <h2> ${character.name} </h2>
      <div>
        <p>${character.species} </p>
        <p> <span class="${character.status.toLowerCase()}"> </span>  ${character.status}</p>
      </div>
      <a href="https://www.google.com"> <button type="button" class="btn btn-primary">Ver Personaje</button> </a>
    `;
    main_container.appendChild(articulo);
  });
};

getCharacters();
