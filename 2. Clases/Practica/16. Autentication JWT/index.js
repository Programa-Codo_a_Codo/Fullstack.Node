const express = require("express");
const port = 3000;
const app = express();

const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const usuarios = []; // simula a una base de datos

const SECRET_KEY = "hola"; // contraseña secreta que usa los tokens

app.use(express.json());

// ---------- CREAR USUARIO ----------
app.post("/crear", async (req, res) => {
  const { username, password } = req.body;

  const userExists = usuarios.find((user) => user.username === username);
  if (userExists)
    return res.status(400).json({ message: "El usuario YA EXISTE !!!" });

  // Generar o encriptar la contraseña
  const hasedPassword = await bcrypt.hash(password, 10);

  // Guardar en la base de datos (lista)
  usuarios.push({ username, password: hasedPassword });

  res.status(201).json({ message: "Usuario REGISTRADO CON EXITO !!!" });
});

// --------- RUTA PROTEGIDA --------
app.get("/protegida", async (req, res) => {
  const token = req.headers["authorization"];

  if (!token)
    return res.status(401).json({ message: "Token NO PROPORCIONADO" });

  try {
    const decoded = jwt.verify(token, SECRET_KEY);
    res
      .status(200)
      .json({ message: "PUDISTE ACCEDER A LA RUTA PROTEGIDA", user: decoded });
  } catch (error) {
    res.status(401).json({ message: "TOKEN NO VALIDO" });
  }
});

// -------------- LOGIN --------------
app.post("/login", async (req, res) => {
  const { username, password } = req.body;

  // Buscar el usuario
  const user = usuarios.find((user) => user.username === username);
  if (!user)
    return res
      .status(400)
      .json({ message: "Usuario y/o contraseña incorrecta" });

  // Buscar y comparar la contraseña
  const isPassword = await bcrypt.compare(password, user.password);
  if (!isPassword)
    return res
      .status(400)
      .json({ message: "Usuario y/o contraseña incorrecta" });

  // Crear token para acceder a las rutas
  const token = jwt.sign({ username }, SECRET_KEY, { expiresIn: "1h" });

  res.status(200).json({ message: "Inicio de sesion exitoso", token });
});

app.get("/", (req, res) => {
  res.send("Bienvenido al server prueba con JWT y BCRYPTS");
});

app.listen(port, () => {
  console.log(`Servidor ejecutandose en el puerto ${port}`);
});
