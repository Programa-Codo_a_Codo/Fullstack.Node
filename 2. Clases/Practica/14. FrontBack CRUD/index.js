// ----- SERVIDOR ESTATICO CON EXPRESS -----
let port = 3000;

const express = require("express");
const app = express();
const usuariosRouter = require("./routes/usuarios");
const path = require('path');


app.use(express.json());

app.use("/usuarios", usuariosRouter);

/*
app.get("/", (req, res) => {
  res.send("Hola desde el localhost:3000");
});
*/

app.use(express.static(path.join(__dirname,'public')));

app.listen(port, () => {
  console.log(`Servidor ejecutandose en el puerto ${port}`);
});
