document.addEventListener("DOMContentLoaded", () => {
  const mostrarCrearUsuarioFormBtn = document.getElementById(
    "mostrarCrearUsuarioFormBtn"
  );
  const crearUsuarioForm = document.getElementById("crearUsuarioForm");
  const editarUsuarioForm = document.getElementById("editarUsuarioForm");
  const listarUsuariosBtn = document.getElementById("listarUsuariosBtn");
  const listaUsuarios = document.getElementById("listaUsuarios");

  // Mostrar u ocultar el form de creación de usuario
  mostrarCrearUsuarioFormBtn.addEventListener("click", () => {
    crearUsuarioForm.classList.toggle("hidden");
  });

  // CREAR USUARIOS
  crearUsuarioForm.addEventListener("submit", async (e) => {
    e.preventDefault();
    const formData = new FormData(crearUsuarioForm);
    const data = {
      nombre: formData.get("nombre"),
      apellido: formData.get("apellido"),
      email: formData.get("mail"),
    };

    const response = await fetch("/usuarios", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    const result = await response.json();
    alert(result.mensaje);

    crearUsuarioForm.reset();
    crearUsuarioForm.classList.add("hidden");
    listarUsuarios();
  });

  // EDITAR USUARIO
  editarUsuarioForm.addEventListener("submit", async (e) => {
    e.preventDefault();
    const formData = new FormData(editarUsuarioForm);
    const id = formData.get("editID");
    const data = {
      nombre: formData.get("editNombre"),
      apellido: formData.get("editApellido"),
      email: formData.get("editMail"),
    };

    const response = await fetch(`/usuarios/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    const result = await response.json();
    alert(result.mensaje);

    editarUsuarioForm.reset();
    editarUsuarioForm.classList.add("hidden");
    listarUsuarios();
  });

  // LISTAR USUARIOS
  listarUsuariosBtn.addEventListener("click", listarUsuarios);

  async function listarUsuarios() {
    const response = await fetch("/usuarios");
    const usuarios = await response.json();
    listaUsuarios.innerHTML = ""; //limpio la lista de usuarios

    usuarios.forEach((usuario) => {
      const li = document.createElement("li");
      li.innerHTML = `
        <span> ID: ${usuario.id}, Nombre: ${usuario.nombre}, Apellido: ${usuario.apellido}, Email: ${usuario.email} </span>
        <div class="actions"> 
          <button class="update" data-id="${usuario.id}" data-nombre="${usuario.nombre}" data-apellido="${usuario.apellido}" data-mail="${usuario.email}" > Actualizar  </button> 
          <button class="delete" data-id="${usuario.id}"> Eliminar </button>
        </div>
      `;

      listaUsuarios.appendChild(li);
    });

    // UPDATE
    document.querySelectorAll(".update").forEach((button) => {
      button.addEventListener("click", (e) => {
        const id = e.target.getAttribute("data-id");
        const nombre = e.target.getAttribute("data-nombre");
        const apellido = e.target.getAttribute("data-apellido");
        const mail = e.target.getAttribute("data-mail");

        document.getElementById("editID").value = id;
        document.getElementById("editNombre").value = nombre;
        document.getElementById("editApellido").value = apellido;
        document.getElementById("editMail").value = mail;

        editarUsuarioForm.classList.remove("hidden");
      });
    });

    // DELETE
    document.querySelectorAll(".delete").forEach((button) => {
      button.addEventListener("click", async (e) => {
        const id = e.target.getAttribute("data-id");
        const response = await fetch(`/usuarios/${id}`, {
          method: "DELETE",
        });

        const result = await response.json();
        alert(result.mensaje);

        listarUsuarios();
      });
    });
  }
});
