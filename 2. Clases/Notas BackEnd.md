## **Base de Datos**

### **Practica de queries simples**

SELECT \* FROM productos LIMIT 5

- Seleccionar todos los productos que tengan como contenido en la descripcion la palabra 'smart'.

SELECT \* FROM productos WHERE descripcion LIKE "%smart%"

- Suma el precio de todos los productos.

SELECT SUM(precio) AS carritoDeCompra FROM productos

- Promedio de precio de todos los productos.

SELECT AVG(precio) AS Promedio_precios FROM productos

- La cantidad de productos que tengo en total.

SELECT COUNT(\*) cantidadProductos FROM productos

- Precio maximo.

SELECT MAX(precio) AS precio_maximo FROM productos

- Stock minimo.

SELECT MIN(stock) AS stock_minimo FROM productos

- Agrupa por nombre del producto y hace una suma del stock total de cada uno.

SELECT nombre, SUM(stock) AS suma_stock_por_nombre
FROM productos
GROUP BY nombre

---

## **NodeJS**

Entorno de ejecucion para correr javascript

- Para ejecutar un script.js con nodejs, ponemos desde cmd o terminal de ejecucion, pero parados en el lugar del archivo.

```js
node script.js
```

---

**Módulos** : Un modulo es un archivo.js que puede importar o exportar funcionalidades para ser utilizadas en otros archivos
Este archivo puede tener varibles, funciones, clases u objetos que pueden ser exportados e importados.
_Un modulo es un archivo javascript que hacer algo_.

Microservicios : Los microservicios son pequeños archivos adicionales que podriamos crear u otras personas podrian crear
y nosotros podriamos consumirlos desde nuestro backend. _En pocas palabras un microservicio es un modulo tambien_.

**Libreria** : _Es un conjunto de archivos javascript. Es decir un conjunto de modulos._

**Framework** : _Es un conjunto de librerias._

### **Tenemos dos tipos de modulos**

- **Modulos nativos** : Son modulos de javascript Ej.: getElementByID(). Exclusivas de javascript.
- **Modulos de nodejs** : NodeJS nos va a ofrecer un conjunto de script, archivos, funciones, clases, etc. Son cosas que ya vienen
  preinstalados cuando instalamos nodejs.
- **Modulos internos** : Son modulos creados por nosotros.
- **Modulos externos** : Son modulos creados por otras personas, en cual debemos importarlos para usarlo.

### **Tipos de servidores**

- **Estaticos** : Se llama servidor estatico, cuando lo que sea lo que esta devolviendo no va a modificar los archivos antes de enviarlos. Es decir devuelve el un archivo prearmado, no modificable. No es que va a estar generando informacion dinamica.

- **Dinamicos** : Se llama servidor dinamico, cuando lo que retorna es con informacion dinamica que cambia sedun el request.
  Por ejemplo: si hago un login a una pagina con cuenta, lo que va retornar es el entorno de la pagina con nuestros datos guardados.

### **NPM**

npm = NODE PACKAGE MANAGER
manager para instalar paquetes de node.

**Algunos comandos para inicial con node**

Crear un proyecto y un package.json

```js
npm init -y
```

_-y para llenar valores por default_

```js
node --watch index.js
```

**Instala express**

```js
npm install express --save
```

_--save instala express y guarda en el json_

**Instala nodemon**

```js
npm install nodemon
```

**Instala mysql**

```js
npm install mysql2
// Esta version (mysql) con no me trajo problemas
npm install mysql
```
