## **HTML**

- Pagina web: Documento estatico
- Sitio web: Conjunto de paginas web -> tiene un dominio
- App web: Negocio

wordpress: es una plataforma para crear sitios web

- https://htmlcheatsheet.com/

## **CSS**

- **Pseudo-clases / Pseudo-Selectores** : Una pseudoclase CSS es una palabra clave que se añade a los selectores y que **especifica un estado especial** del elemento seleccionado.

Ejemplo:

```css
a:hover {
  color: red;
}
```

- **Pseudo-elementos** : Los pseudoelementos se utilizan para **añadir estilos a una parte concreta del documento.** sin necesidad de modificar el HTML subyacente.

Ejemplo

```css
.title-page::after {
  content: "texto que se agrega DESPUES del title";
}

.title-page::before {
  content: "texto que se agrega ANTES del title";
}
```

### **¿ Que diferencia tiene : y :: y como se relaciona con los pseudoelementos y psudoclases ?**

:: es la sintaxis utilizada en CSS3 y versiones posteriores para seleccionar y estilizar el pseudo-elemento after. La doble columna (::) se introdujo para distinguir los pseudo-elementos de los pseudo-selectores (como :hover o :active).

https://animate.style/

### **Transiciones y animaciones**

- Transicion necesitan algo (alguna interaccion) para funcionar. La animacion es automatica.

## **Bootstrap**

**Material multimedia**

- Instalación via CDN: https://youtu.be/LoHV4O8PLXc
- Uso y funcionamiento: https://drive.google.com/file/d/1z8QLWJk9cf-hOBDTZoYbppD48Rtq-7fM/view?usp=sharing
- Cuadrícula en Bootstrap: https://drive.google.com/file/d/1z9oH2sYSftYfnH8AMHpceqirEXiL3y4w/view?usp=sharing
- Formularios y Botones: https://drive.google.com/file/d/1qe9CFGIFtpJP2gHWX5ItCkgzcwzAmRz4/view?usp=sharing
- Usuario y Clave: https://drive.google.com/file/d/18YhKItqDauZd_eH_HIRxj-P1ODUrh6-q/view?usp=sharing

## **GIT**

**Material multimedia**

- Instalación de GIT: https://youtu.be/ptXiQwE535s
- Comandos básicos para la terminal, moverse entre directorios: https://youtu.be/SIotISUq-3w
- Comandos básicos para la terminal, crear carpetas, archivos, eliminarlos: https://youtu.be/W-z0iTXEarw
- Inicializar carpeta y configurar usuarios: https://youtu.be/KAY1j-MPpyA
- Subir archivo a un repositorio local: https://youtu.be/NRaLP7vtoOY
- Subir archivos a GitHub: https://youtu.be/9p7Avpcs7Bk
- Clonar repositorios: https://youtu.be/AKp_q9hdJRI
- Cómo publicar un sitio web gratis con Github Pages: https://youtu.be/9p7Avpcs7Bk

## **JavaScript**

https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Array

### **Sincrono y Asincrono**

En sincronico los proceso corren a la par (mismo tiempo), esto puede demorar las tareas ya que si hay dependencia entre ellas, una no puede seguir porque el otro proceso no termino.

En Asincronismo, los procesos corren en paralelo (tiempo distinto) y con esto el programa puede seguir corriendo o haciendo tareas mientras al mismo tiempo espera a que termine un proceso.

Asincronismo hacer muchas tareas ejecuten en simultaneo sin tener que estar dependiendo una de la otra.
Cuando tenemos un proceso bloqueante (que tarda demasiado para que pueda seguir con el resto de los procesos), nos combiene trabajar con async ya que lo envia a Event Loop para trabaje con el y mientras tanto el callstack trabaja con los procesos No bloqueantes.
