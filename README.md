# **Fullstack.Node**

- **Tutor** : Gonzalo Monicat | 📨 gonzalo.monicat@bue.edu.ar | 📋 [Canal de consultas](https://forms.gle/MKNgCJGnPFpSShzA9)
- **Instructor** : Marcos Sosa | 📨 marc.sosa@bue.edu.ar | 🔗 [Linkedin](www.linkedin.com/in/marcossosa77) | 📱 [+54 9 11 2712-2853](phone)
- **Comisión** : 24259
- **Días** : Martes y Jueves de 18 a 19.30
- **Enlaces** :
  [Aula Virtual](https://aulasvirtuales.bue.edu.ar) |
  [Meet](https://meet.google.com/wrg-nbmm-pjt) |
  [Drive](https://drive.google.com/drive/folders/1rtkjAs4zR2MJ7r796_XeCwHml-0uyRmk) |
  [Clases Grabadas](https://www.youtube.com/playlist?list=PLCweTlGmKpFoS-6DdNRHGqEEVHJdszusE) |
  [Asistencia](https://forms.gle/NN3VZGbJCTtaTDze8) |
  [Grupo de WhatsApp (sin tutor)](https://chat.whatsapp.com/CQvEh81eNDf3mQrwRZ0UHk) |
  [Grupo de WhatsApp (Oficial)](https://chat.whatsapp.com/C1TKigcSlId3iLqfJWBqpR)

- **Clases de apoyo** :
  🕒 Viernes de 15 a 17 hs |
  [Meet](https://meet.google.com/sjw-gkny-wam) |
  [Clases Grabadas](https://youtube.com/playlist?list=PLCL4VN3IMxOTqBIvTV191gTdIjt5Ym73n)

---

## **Guia de Estudio**

1. [Guía del estudiante](/1.%20Guia%20de%20estudio/1.%20Guia%20del%20estudiante.pdf "Guia del estudiante")
2. [Plan de estudio](/1.%20Guia%20de%20estudio/2.%20Plan%20de%20estudios.pdf "Plan de estudio")

---

## **Documentación Aula Virtual**

### **FrontEnd**

1. [Documentación HTML](/2.%20Clases/Documentacion%20Aula%20Virtual/FrontEnd/1.%20HTML/ "Documentación HTML")
2. [Documentación CSS](/2.%20Clases/Documentacion%20Aula%20Virtual/FrontEnd/2.%20CSS/ "Documentación CSS")
3. [Documentación Bootstrap](/2.%20Clases/Documentacion%20Aula%20Virtual/FrontEnd/3.%20Bootstrap/ "Documentación Bootstrap")
4. [Documentación GIT](/2.%20Clases/Documentacion%20Aula%20Virtual/FrontEnd/4.%20GIT/ "Documentación GIT")
5. [Documentación Scrum](/2.%20Clases/Documentacion%20Aula%20Virtual/FrontEnd/5.%20Scrum/ "Documentación Scrum")
6. [Documentación JavaScript](/2.%20Clases/Documentacion%20Aula%20Virtual/FrontEnd/6.%20JavaScript/ "Documentación JavaScript")
7. [Documentación VUE](/2.%20Clases/Documentacion%20Aula%20Virtual/FrontEnd/7.%20VUE/ "Documentación VUE")
8. [Documentación Arquitectura](/2.%20Clases/Documentacion%20Aula%20Virtual/FrontEnd/8.%20Arquitectura/ "Documentación Arquitectura")

### **BackEnd**

1. [Documentación Base de Datos](/2.%20Clases/Documentacion%20Aula%20Virtual/BackEnd/1.%20Base%20de%20Datos/ "Documentación Base de Datos")
1. [Documentación NodeJS](/2.%20Clases/Documentacion%20Aula%20Virtual/BackEnd/2.%20NodeJS/ "Documentación NodeJS")

---

## **Documentación Extra**

1. [Instalación Visual Studio Code](/2.%20Clases/Documentacion%20Extra/1.%20Instalación%20Visual%20Studio%20Code.pdf "Instalación Visual Studio Code")

---

## **Practica**

1. [Practica HTML](/2.%20Clases/Practica/1.%20HTML/ "Practica HTML")
2. [Practica CSS](/2.%20Clases/Practica/2.%20CSS/ "Practica CSS")
3. [Practica Grid layout](/2.%20Clases/Practica/3.%20Grid%20layout/ "Practica Grid layout")
4. [Practica Portfolio by profe](/2.%20Clases/Practica/4.%20Portfolio%20by%20profe/ "Practica Portfolio by profe")
5. [Practica Transitions - Animations](/2.%20Clases/Practica/5.%20Transitions%20-%20Animations/ "Practica Transitions - Animations")
6. [Practica Dark Mode](/2.%20Clases/Practica/6.%20Dark%20Mode/ "Practica Dark Mode")
7. [Practica Validation](/2.%20Clases/Practica/7.%20Validation/ "Practica Validation")
8. [Practica Promise - Async Await](/2.%20Clases/Practica/8.%20Promise%20-%20Async%20Await/ "Practica Promise - Async Await")
9. [Practica Fetch - Rick and Morty](/2.%20Clases/Practica/9.%20Fetch%20-%20Rick%20and%20Morty/ "Practica Fetch - Rick and Morty")
10. [Practica NodeJS](/2.%20Clases/Practica/10.%20NodeJS/ "Practica NodeJS")
11. [Practica Express](/2.%20Clases/Practica/11.%20Express/ "Practica Express")
12. [Practica Endpoints](/2.%20Clases/Practica/12.%20Endpoints/ "Practica Endpoints")
13. [Practica CRUD with db](/2.%20Clases/Practica/13.%20CRUD%20with%20db/ "Practica CRUD with db")
14. [Practica FrontBack CRUD](/2.%20Clases/Practica/14.%20FrontBack%20CRUD/ "Practica FrontBack CRUD")
15. [Practica Multer](/2.%20Clases/Practica/15.%20Multer/ "Practica Multer")
16. [Practica Auntentication JWT](/2.%20Clases/Practica/16.%20Autentication%20JWT/ "Practica Auntentication JWT")

---

## **Ejercicios Obligatorios**

### **FrontEnd**

1. [Descubriendo la plataforma](/3.%20Ejercicios%20Obligatorios/1%20-%20Descubriendo%20la%20plataforma.png "Descubriendo la plataforma")
2. [HTML](/3.%20Ejercicios%20Obligatorios/FrontEnd/2%20-%20HTML.png "HTML")
3. [CSS I](/3.%20Ejercicios%20Obligatorios/FrontEnd/3%20-%20CSS%20I.png "CSS I")
4. [CSS II](/3.%20Ejercicios%20Obligatorios/FrontEnd/4%20-%20CSS%20II.png "CSS II")
5. [CSS III](/3.%20Ejercicios%20Obligatorios/FrontEnd/5%20-%20CSS%20III.png "CSS III")
6. [Bootstrap](/3.%20Ejercicios%20Obligatorios/FrontEnd/6%20-%20Bootstrap.png "Bootstrap")
7. [JavaScript I](/3.%20Ejercicios%20Obligatorios/FrontEnd/7%20-%20JavaScript%20I.png "JavaScript I")
8. [JavaScript II](/3.%20Ejercicios%20Obligatorios/FrontEnd/8%20-%20JavaScript%20II.png "JavaScript II")
9. [JavaScript III](/3.%20Ejercicios%20Obligatorios/FrontEnd/9%20-%20JavaScript%20III.png "JavaScript III")
10. [VUE](/3.%20Ejercicios%20Obligatorios/FrontEnd/10%20-%20VUE.png "VUE")

[Examen Integrador FrontEnd](/3.%20Ejercicios%20Obligatorios/FrontEnd/Examen%20Integrador%20Front%20End.png "Examen Integrador FrontEnd")

### **BackEnd**

1. [Base de Datos I](/3.%20Ejercicios%20Obligatorios/BackEnd/1.%20Base%20de%20Datos%20I.png "Base de Datos I")
2. [Base de Datos II](/3.%20Ejercicios%20Obligatorios/BackEnd/2.%20Base%20de%20Datos%20II.png "Base de Datos II")
3. [NodeJS I](/3.%20Ejercicios%20Obligatorios/BackEnd/ "NodeJS I")
4. [NodeJS II](/3.%20Ejercicios%20Obligatorios/BackEnd/4.%20NodeJS%20II.png "NodeJS II")
5. [NodeJS III](/3.%20Ejercicios%20Obligatorios/BackEnd/5.%20NodeJS%20III.png "NodeJS III")
6. [NodeJS IV](/3.%20Ejercicios%20Obligatorios/BackEnd/6.%20NodeJS%20IV.png "NodeJS IV")
7. [NodeJS V](/3.%20Ejercicios%20Obligatorios/BackEnd/7.%20NodeJS%20V.png "NodeJS V")
8. [NodeJS VI](/3.%20Ejercicios%20Obligatorios/BackEnd/8.%20NodeJS%20VI.png "NodeJS VI")

[Examen Integrador BackEnd](/3.%20Ejercicios%20Obligatorios/BackEnd/Examen%20Integrador%20BackEnd.png "Examen Integrador BackEnd")

---

## **Trabajos Practicos**

- [Entrega TPs](./4.%20Trabajos%20Practicos/Entrega%20TPs/ "Entrega TPs")
- [Entregas Grupal](./4.%20Trabajos%20Practicos/Entregas%20Grupal/ "Entregas Grupal")
