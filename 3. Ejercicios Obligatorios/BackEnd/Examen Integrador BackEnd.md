# **Examen Integrador BackEnd**

- **¿Cuál de los siguientes no es un tipo de datos soportado por MySQL?**

  - Float
  - Varchar
  - NaN ✔
  - Int

- **¿Cuál es el comando correcto para agregar una nueva columna a una tabla existente en MySQL?**

  - `ALTER TABLE nombretabla ADD COLUMN nombre_columna tipo_de_dato` ✔
  - `UPDATE TABLE nombretabla ADD COLUMN nombre_columna tipo_de_dato`
  - `INSERT COLUMN nombre_columna tipo_de_dato TO nombretabla`
  - `ADD COLUMN nombre_columna tipo_de_dato TO nombretabla`

- **¿Cuál es la instrucción correcta para actualizar un registro específico en una tabla en SQL?**

  - `UPDATE nombretabla SET column='valor' WHERE condition` ✔
  - `MODIFY nombretabla SET column='valor' WHERE condition`
  - `ALTER nombretabla SET column='valor' WHERE condition`
  - `CHANGE nombretabla SET column='valor' WHERE condition`

- **¿Cuál de las siguientes afirmaciones sobre los módulos en Node.js no es correcta?**

  - Los módulos en Node.js se cargan usando la función require.
  - `module.exports` se usa para exportar objetos, funciones o variables desde un módulo.
  - Los módulos pueden ser archivos locales o paquetes de node_modules.
  - El sistema de módulos de Node.js sigue el formato de módulos ES6 por defecto. ✔

- **¿Cuál de los siguientes códigos de estado HTTP indica que la solicitud ha tenido éxito?**

  - 500 Internal Server Error
  - 404 Not Found
  - 401 Unauthorized
  - 200 OK ✔

- **¿Cuál es la función de la librería Nodemon?**

  - Para cambiar datos del servidor
  - Automatizar el cambio para la continuidad de ejecución del servidor en tiempo real ✔
  - Para levantar un servidor local inicialmente

- **¿Cuál de las siguientes opciones no es una característica de Node.js?**

  - Node.js es adecuado para aplicaciones en tiempo real.
  - Node.js utiliza un modelo de E/S no bloqueante.
  - Node.js usa el motor V8 de Google para ejecutar JavaScript.
  - Node.js es adecuado para tareas de CPU intensivas. ✔

- **¿Qué entendemos por JWT?**

  - Una extensión de archivo incompatible con nuestros programas.
  - Es el término de una nueva vulnerabilidad de seguridad de sitios web.
  - Un token para autenticación y autorización en webs y aplicaciones móviles. ✔
  - Un middleware que me permite comprimir imágenes desde el servidor.

- **¿Cuál de las siguientes afirmaciones sobre la gestión de paquetes en Node.js no es correcta?**

  - `npm uninstall` actualiza el paquete especificado a la última versión. ✔
  - `package.json` es un archivo de configuración de paquetes en Node.js.
  - `npm install` descarga e instala el paquete especificado.
  - npm es el gestor de paquetes predeterminado para Node.js.

- **¿Qué es un middleware?**

  - Un método HTTP
  - Un bloque de código
  - Una variable
  - Una función ✔
