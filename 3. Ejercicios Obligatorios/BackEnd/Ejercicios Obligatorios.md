# **Preguntas y Respuestas - Ejercicios Obligatorios**

## **Base de Datos I**

- **Una base de datos es una colección de datos relacionados que reflejan un mini-mundo que nos interesa.**

  - Verdadero ✔
  - Falso

- **El lenguaje DDL:**

  - Es un lenguaje del sistema operativo
  - Es el lenguaje de definición de datos ✔
  - Es un lenguaje que se usa para crear los programas de acceso a los datos
  - Es un decodificador de lenguaje
  - Es el lenguaje que usa el DBA

- **Una entidad simboliza una:**

  - Tabla ✔
  - Relación
  - Atributo

- **El atributo que sirve para identificar unívocamente una tupla en una tabla es:**

  - Cardinalidad
  - Clave ✔
  - Clave foránea

- **El modelo relacional representa a la base de datos como una colección de "entidades" relacionadas entre sí.**

  - Verdadero ✔
  - Falso

## **Base de Datos II**

- **Para crear objetos de una base de datos se utiliza:**

  - AVG
  - ALTER
  - CREATE ✔
  - INSERT

- **Para obtener el máximo valor de un grupo de datos de una o varias tablas de la base de datos se utiliza:**

  - GROUP BY
  - MAX ✔
  - SELECT ✔
  - HAVING

- **Para modificar los datos de una tabla de una base de datos se utiliza:**

  - UPDATE ✔
  - DROP
  - SUM
  - DELETE

- **Para agregar datos en una tabla de una base de datos se utiliza:**

  - INSERT ✔
  - AVG
  - CREATE
  - ALTER

- **Para obtener información de una base de datos se utiliza:**

  - GROUP BY
  - HAVING
  - MAX
  - SELECT ✔

- **Para obtener el promedio a partir de los datos de una base de datos se utiliza:**

  - HAVING
  - GROUP BY
  - SELECT
  - AVG ✔

- **Para eliminar un objeto de una base de datos se utiliza:**

  - DELETE
  - UPDATE
  - DROP ✔
  - SUM

- **Para sumar datos de una base de datos se utiliza:**

  - SELECT ✔
  - SUM ✔
  - GROUP BY
  - HAVING

- **Para modificar un objeto de una base de datos se utiliza:**

  - ALTER ✔
  - CREATE
  - AVG
  - INSERT

- **Si se rompió el equipo donde guardaba mi base de datos debo hacer una...**

  - restauración ✔
  - role
  - stored procedure
  - backup
  - vista
  - revoke

- **Para proteger mi base de datos de cualquier tipo de catástrofe uso un...**

  - vista
  - restauración
  - backup ✔
  - revoke
  - stored procedure
  - role

- **Para automatizar algunas tareas a realizarse usando datos de diferentes tablas se debe usar un...**

  - vista
  - stored procedure ✔
  - restauración
  - role
  - revoke
  - backup

- **¿Qué opción utilizo para indicar la selección de un grupo de datos?**

  - Indicate
  - Where ✔
  - Ninguna de las otras respuestas es correcta
  - Whereis

- **¿Qué implica que una base de datos sea relacional?**

  - Que una tabla puede relacionarse con un lenguaje
  - Que la base de datos se relaciona con un lenguaje
  - Que una o varias tablas pueden relacionar os datos entre si ✔
  - Que la base de datos se relaciona con una tabla

- **¿Una misma columna de una tabla puede contener diferentes tipos de datos?**

  - No, solamente acepta un dato por columna ✔
  - Si, puedo incorporar diferentes datos en la columna

## **NodeJS I**

- FALTO COMPLETAR

## **NodeJS II**

- **¿Qué contiene el archivo package.json?**

  - Contiene todos los datos y registros generados por la aplicación
  - Contiene la configuración que define las dependencias y scripts de un proyecto Node.js ✔
  - Contiene las credenciales de usuario para acceder a la base de datos
  - Contiene la definición las rutas de la aplicación en un proyecto Node.js
  - Contiene la definición los estilos CSS utilizados en un proyecto Node.js

- **¿Qué es Express JS?**

  - Es un framework de aplicaciones web mínimo y flexible en el entorno de NodeJS ✔
  - Es una herramienta de línea de comandos para la gestión de paquetes en NodeJS
  - Es una biblioteca gráfica para crear interfaces de usuario en aplicaciones web
  - Es una base de datos utilizada para almacenar datos en aplicaciones NodeJS
  - Es un entorno de desarrollo integrado (IDE) para escribir y depurar código JavaScript

- **¿Qué es un servidor web?**

  - En términos de software, se refiere a cualquier programa o aplicación que proporciona servicios o recursos a otros dispositivos o programas en una red ✔
  - Un servidor web es un servicio que permite la transmisión de datos exclusivamente a través de redes privadas
  - Un servidor web es una aplicación de escritorio utilizada para diseñar páginas web
  - Un servidor web es una base de datos que almacena información de usuarios y contraseñas
  - Un servidor web es un dispositivo físico que almacena y procesa grandes cantidades de datos sin conexión a internet

- **En el protocolo HTTP, ¿cuáles son los 5 métodos más usados?**

  - GET, CONNECT, PUT, DELETE y PATCH
  - GET, POST, PUT, MODIFY y DELETE
  - GET, POST, PUT, DELETE y PATCH ✔
  - FETCH, POST, PUT, DELETE y TRACE

- **En un servidor estático nativo ¿qué parámetros recibe en cada llamada?**

  - El nombre del archivo y la ruta del archivo
  - El tipo de contenido y la longitud del contenido
  - La solicitud y la respuesta ✔
  - El puerto y el protocolo
  - El método HTTP y la cabecera de autorización

- **Un servidor dinámico consiste en un servidor web estático con software adicional, como una aplicación de servidor y una base de datos. Se le dice dinámico porque la aplicación actualiza los archivos alojados, antes de enviar el contenido al navegador del cliente mediante el servidor HTTP**

  - Verdadero ✔
  - Falso

## **NodeJS III**

- **¿Cuál de las siguientes es una ruta válida en Express.js para obtener una lista de usuarios?**

  - `app.put('/users', (req, res) => { ... });`
  - `app.patch('/users', (req, res) => { ... });`
  - `app.delete('/users', (req, res) => { ... });`
  - `app.post('/users', (req, res) => { ... });`
  - `app.get('/users', (req, res) => { ... });` ✔

- **¿Cuál de las siguientes opciones no es un método HTTP estándar?**

  - POST
  - GET
  - PUT
  - UPDATE ✔
  - DELETE

- **¿Cuál es el resultado esperado de una solicitud HTTP DELETE exitosa?**

  - Código de estado 404 y el recurso eliminado
  - Código de estado 200 o 204 y el recurso eliminado ✔
  - Código de estado 500 y el recurso eliminado
  - Código de estado 201 y el recurso creado
  - Código de estado 200 y el recurso actualizado

- **¿Cuál es la función principal del objeto req en Express.js?**

  - Contener la solicitud HTTP y los datos enviados por el cliente ✔
  - Manipular la base de datos directamente
  - Enviar datos al cliente
  - No existe el objeto req en Express
  - Configurar las rutas del servidor

- **¿Qué método HTTP se usa generalmente para obtener datos de un servidor?**

  - DELETE
  - PATCH
  - PUT
  - POST
  - GET ✔

- **En Express.js, ¿cuál es el propósito del middleware?**

  - Ejecutar código después de que el servidor haya enviado una respuesta
  - Ejecutar código antes de que el servidor reciba una solicitud
  - Ejecutar código durante el ciclo de vida de la solicitud/respuesta ✔
  - Solo para autenticar usuarios
  - Para enviar datos al cliente

## **NodeJS IV**

- **¿Cuál de las siguientes opciones es una configuración básica de Multer para definir la carpeta de destino de los archivos subidos?**

  - `storage: multer.memoryStorage({ destination: 'uploads/' })`
  - `storage: multer.fileStorage({ destination: 'uploads/' })`
  - `storage: multer.uploadStorage({ destination: 'uploads/' })`
  - `storage: multer.saveStorage({ destination: 'uploads/' })`
  - `storage: multer.diskStorage({ destination: 'uploads/' })` ✔

- **¿Cuál de las siguientes opciones muestra la forma correcta de realizar una consulta utilizando el paquete mysql en Node.js?**

  - `connection.querySQL('SELECT * FROM users', function(error, results) {})`
  - `connection.sendQuery('SELECT * FROM users', function(error, results) {})`
  - `connection.runQuery('SELECT * FROM users', function(error, results) {})`
  - `connection.execute('SELECT * FROM users', function(error, results) {})`
  - `connection.query('SELECT * FROM users', function(error, results) {})` ✔

- **¿Cuál es el propósito principal de Multer en una aplicación Node.js?**

  - Manejar la autenticación de usuarios.
  - Enviar correos electrónicos.
  - Gestionar la subida de archivos. ✔
  - Conectar a una base de datos MySQL.
  - Realizar validaciones de formularios.

- **¿Cuál es el propósito principal del paquete mysql en una aplicación Node.js?**

  - Realizar cálculos matemáticos complejos.
  - Enviar mensajes en tiempo real.
  - Crear interfaces gráficas de usuario.
  - Gestionar la subida de archivos.
  - Conectar y realizar operaciones en una base de datos MySQL. ✔

- **¿Qué método del paquete mysql se utiliza para establecer una conexión a la base de datos?**

  - `mysql.openConnection()`
  - `mysql.createQuery()`
  - `mysql.initConnection()`
  - `mysql.createConnection()` ✔
  - `mysql.connect()`

- **¿Qué tipo de middleware es Multer en una aplicación Express?**

  - Middleware de enrutamiento.
  - Middleware de autenticación.
  - Middleware de manejo de archivos. ✔
  - Middleware de autorización.
  - Middleware de seguridad.

## **NodeJS V**

- **¿Cuál es el propósito de la clave secreta en JWT?**

  - Para almacenar la configuración del servidor.
  - Para definir las políticas de acceso del usuario.
  - Para determinar la duración de la sesión del usuario.
  - Para firmar y verificar la integridad de los tokens. ✔
  - Para cifrar las contraseñas de los usuarios.

- **¿Qué es la autenticación en el contexto de la seguridad informática?**

  - La autenticación es el proceso de diseñar la interfaz de usuario.
  - La autenticación es el proceso de cifrar datos para protegerlos.
  - La autenticación es el proceso de respaldar datos en una ubicación segura.
  - La autenticación es el proceso de verificar la identidad de un usuario.
  - La autenticación es el proceso de optimizar el rendimiento del servidor.

- **¿Qué método se utiliza para generar un token JWT en Node.js usando la biblioteca jsonwebtoken?**

  - `jwt.make()`
  - `jwt.sign()` ✔
  - `jwt.generate()`
  - `jwt.create()`
  - `jwt.issue()`

- **¿Qué parte del JWT contiene las reclamaciones (claims) del usuario?**

  - Header
  - Payload ✔
  - Signature
  - Header y Signature
  - Token

- **¿Qué respuesta debe dar el servidor si no se proporciona un token JWT en la solicitud a una ruta protegida?**

  - 500 Internal Server Error
  - 200 OK
  - 403 Forbidden ✔
  - 201 Created
  - 404 Not Found

- **¿Qué significa JWT en el contexto de la autenticación?**

  - JSON Web Token ✔
  - JavaScript Widget Token
  - JavaScript Web Token
  - Java Token Wrapper
  - Java Web Token
