# **Preguntas y Respuestas - Ejercicios Obligatorios**

## **Descubriendo la plataforma**

- **En la plataforma educativa podrás:**

  - Acceder a los contenidos de la unidad
  - Acceder a los ejercicios obligatorios
  - Comunicarte con el docente
  - Realizar el examen integrador
  - Todas las respuestas son correctas ✔

- **La Cartelera de Novedades sirve para:**

  - Que el docente le responda a un compañero
  - Que el docente, facilitador o administrativo comunique información general ✔
  - Que un compañero realice una consulta

- **El Espacio de consultas sirve para:**

  - Que posteemos cualquier tipo de comentario
  - Que realicemos consultas y el docente las responda ✔
  - Que intercambiemos información con los compañeros sin intervención del docente

- **Las actividades y ejercicios pueden ser:**

  - Solo optativas
  - Solo obligatorias
  - Optativas y Obligatorias ✔

- **Las condiciones de cursada son:**

  - Aprobar todos los ejercicios obligatorios y el examen integrador
  - Resolver todos los ejercicios obligatorios y aprobar el examen integrador ✔
  - Aprobar el examen integrador
  - Aprobar los ejercicios obligatorios y optativos

- **Los Ejercicios Obligatorios:**

  - Se realizan en una sola fecha, tienen 1 (un) intento y el tiempo para realizarlo es de 4 horas
  - Se realizan en dos fechas diferentes, tienen 2 intentos y duran 40 minutos
  - Se realizan en cualquier fecha, tienen varios intentos y no tienen tiempo de duración
  - Se realizar en una fecha específica, tienen un tiempo de duración y tienen 2 (dos) intentos ✔

---

## **HTML**

- **El código `<input type="text" name="nombre" id="nombre"` dará como resultado en HTML:**

  - Un cuadro de texto reservado para ingresar el nombre ✔
  - Un botón para enviar los datos del formulario
  - Un botónpara poner en blanco los datos del formulario

- **¿Cuál consideras que debe ser la herramienta a utilizar para mostrar datos que tiene sentido de conjunto en HTML?**

  - Una tabla
  - Un cuadro desplegable
  - Una lista ✔

- **¿Qué opciones están consideradas obsoletas en HTML?**

  - La etiqueta `<div></div>`
  - La etiqueta `<font></font>` ✔
  - La etiqueta `<form></form>`

- **¿Para qué sirve la etiqueta `<th></th>` en una tabla?**

  - Para definir el header de la tabla
  - Para colocar una celda de contenido
  - Para colocar una celda de encabezado ✔

- **¿Qué formato de imagen debo utilizar si esta tiene transparencias?**

  - PNG ✔
  - WEBP
  - SVG
  - JPEG

---

## **CSS I**

- **Si quiero vincular una hoja de estilos externa CSS que debo hacer:**

  - Escribir los estilos dentro de la etiqueta `<style></style>` en el head del HTML
  - Agregar un atributo `style=""` a la etiqueta en cuestión
  - Colocar una etiqueta `<link />` con el atributo href que vincule el archivo .css externo ✔

- **¿Qué selector CSS tiene más precedencia?**

  - ID ✔
  - Etiqueta
  - Clase

- **En CSS puedo combinar selectores para adquirir mayor especificidad**

  - Verdadero ✔
  - Falso

- **Cuando las propiedades de 2 selectores con misma especificidad entran en conflicto, ¿Cuál es la que prevalece?**

  - La que se encuentra última ✔
  - La que se encuentra primero

---

## **CSS II**

- **¿Cuáles de estas opciones pertenecen al Box Modeling? Seleccione una o más de una:**

  - border ✔
  - margin ✔
  - content ✔
  - font-size
  - background-color

- **Todos los navegadores tienen el mismo estilo por eso no hace falta normalizarlos**

  - Verdadero
  - Falso ✔

  _Cada navegador tiene su propia implementación de estilos por defecto, por eso es importante hacer un reset de estos para que nuestro sitio se vea igual en todos los navegadores._

- **¿En cuántas dimensiones se puede organizar nuestro contenido con flexbox al mismo tiempo?**

  - En 2, el eje X y el eje Y a la misma vez
  - En los 3 ejes, X, Y y Z
  - Sola una, en el eje X o en el eje Y ✔

- **¿Qué propiedad de flexbox debería utilizar si quiero distribuir mis elementos a lo ancho de su contenedor separados de forma uniforme y sin espacios a los costados?**

  - Con la propiedad `align-items: flex-start;`
  - Con la propiedad `justify-content: space-between;` ✔
  - Con la propiedad `items: distribute;`

- **¿Qué es Grid Layout?**

  - Es el reemplazo de las tablas tradicionales en HTML
  - Grid Layout es un sistema de 2 dimensiones que me permite crear grillas predeterminadas para ubicar mis elementos ✔
  - Ninguna de las anteriores

- **¿Con Grid Layout es posible posicionar mis grid-items individualmente?**

  - Si, con las propiedades `align-items` y `justify-items`
  - No es posible
  - Si, con las propiedades `align-self` y `justify-self` ✔

---

## **CSS III**

- **¿Cuál es el comportamiento del `position: relative;`?**

  - Mueve un elemento desde su posición original sin modificar el flujo de la página ✔
  - Mueve un elemento desde el body o su contenedor padre y remueve el espacio que ocupaba en el sitio
  - Deja el elemento fijo en un sitio de la página que nos acompañará cuando hagamos scroll

- **La pseudoclase :hover es un evento que se activa cuando pasamos el mouse por un elemento de nuestro sitio:**

  - Verdadero ✔
  - Falso

- **¿Cuál es la unidad de medida para definir el ángulo de rotación en la transformación rotate?**

  - `grads`
  - `degress` ✔
  - `radians`

- **¿Qué es el estilo de diseño mobile-first?**

  - Tener 2 sitios desarrollados en paralelo, uno para celulares y otro para escritorio
  - Una metodología responsive donde se comienza a diseñar nuestro sitio desde la vista en celulares hasta la vista desktop ✔
  - Ninguna de las anteriores

- **¿Cuál es la regla CSS utilizada para crear una animación?**

  - `@keyframes` ✔
  - `@animation`
  - `@media`

---

## **Bootstrap**

- **Es Bootstrap un:**

  - Lenguaje de programación
  - Framework ✔
  - IDE

- **¿Cuál es la cantidad de columnas totales definidas por Bootstrap?**

  - 16
  - 12 ✔
  - 8

- **¿Bootstrap puede funcionar en forma local sin conexión a internet?**

  - No
  - Si ✔

- **Un elemento con la clase `col-md-4` ¿Cuántas columnas ocupará y en qué tamaños?**

  - 4 columnas desde el breakpoint medium en adelante ✔
  - 4 columnas solo en el breakpoint medium y todas en el resto
  - 4 columnas desde el 0 hasta el breakpoint medium

- **Selecciones cuál de las siguientes opciones son componentes de Bootstrap:**

  - Modal ✔
  - Navbar ✔
  - Tabla
  - Carousel ✔
  - Headings

---

## **JavaScript I**

- **¿Dentro de qué elemento `HTML` ponemos JavaScript?**

  - `<js>`
  - `<script>` ✔
  - `<scripting>`
  - `<javascript>`

- **¿Dónde está el lugar correcto para insertar un JavaScript?**

  - Tanto la sección `<head>` como la sección `<body>` ✔
  - La sección `<body>`
  - La sección `<head>`

- **¿Cuál es la sintaxis correcta para hacer referencia a un script externo llamado `"xxx.js"`?**

  - `<script src= "xxx.js">` ✔
  - `<script href= "xxx.js">`
  - `<script name = "xxx.js">`

- **El archivo JavaScript externo debe contener la etiqueta `<script>`**

  - Verdadero
  - Falso ✔

- **¿Cómo se escribe `"Hola mundo"` en un cuadro de alerta?**

  - `alertBox("Hola mundo");`
  - `msgBox("Hola mundo");`
  - `alert("Hola mundo");` ✔
  - `msg("Hola mundo");`

- **¿Cómo se escribe la declaración `if` en JavaScript?**

  - `if (i == 5)` ✔
  - `if i = 5`
  - `if i == 5 then`
  - `if i = 5 then`

- **¿Cómo escribir una instrucción `IF` para ejecutar algún código si `"i"` NO es igual a 5?**

  - `if i <> 5`
  - `if i =! 5 then`
  - `if (i ! = 5)` ✔
  - `if (i <> 5)`

- **¿Cómo comienza un bucle `WHILE`?**

  - `while (i <= 10)` ✔
  - `while i = 1 to 10`
  - `while (i <= 10; i++)`

- **¿Cómo comienza un bucle `FOR`**

  - `for (i = 0; i <= 5; i++)` ✔
  - `for (i <= 5; i ++)`
  - `for (i = 0; i <= 5)`
  - `for i = 1 a 5`

- **¿Cómo se puede agregar un comentario en un JavaScript?**

  - `<!-- Esto es un comentario -->`
  - `'Esto es un comentario`
  - `//Este es un comentario` ✔

---

## **JavaScript II**

- **¿Qué ítem se relaciona con el concepto de `Scope`?**

  - Es un método que llama al callback
  - Es el contexto actual de ejecución ✔
  - Es una función que permite acceder al ámbito de una función exterior desde una función interior

- **¿Qué ítem se relaciona con el concepto de `Reduce`?**

  - Es un método que llama al callback ✔
  - Es el contexto actual de ejecución
  - Es una función que permite acceder al ámbito de una función exterior desde una función interior

- **¿Qué ítem se relaciona con el concepto de `Closure`?**

  - Es un método que llama al callback
  - Es el contexto actual de ejecución
  - Es una función que permite acceder al ámbito de una función exterior desde una función interior ✔

- **¿Cuál es la forma correcta de escribir el arrow function de: `function (a){ return a + 100;}`**

  - `a+100<=a;`
  - `a=>a+100;` ✔
  - `a+100=>a;`

- **El operador `||` corresponde a:**

  - then
  - Y
  - O ✔

- **¿Cómo se crea una función en JavaScript?**

  - `function myFunction ()` ✔
  - `function = myFunction ()`
  - `function : myFunction ()`

- **¿Cómo se llama a una función llamada `"myFunction"`?**

  - call myFunction ()
  - myFunction () ✔
  - call function myFunction ()

- **¿Qué muestra por consola el siguiente código?**

  ```js
  let suma = 0;

  for (let i = 0; i <= 5; i++) {
    suma = suma + i;
  }

  console.log(i);
  ```

  - Muestra la suma de los números del 1 al 5
  - Error i no está definido ✔
  - Muestra del 0 al 5
  - Muestra del 1 al 5

- **¿Qué muestra por consola el siguiente código?**

  ```js
  let suma = 0;

  for (var i = 0; i <= 5; i++) {
    suma = suma + i;
  }

  console.log(i);
  ```

  - Muestra del 1 al 5
  - Error i no está definido
  - Muestra la suma de los números del 1 al 5
  - Muestra del 0 al 5
  - 6 ✔

- **¿Qué valor muestra por consola el siguiente código?**

  ```js
  fc = (a) => a + 100;

  console.log(fc(10));
  ```

  - 100
  - 10
  - 110 ✔

---

## **JavaScript III**

- **El elemento `<button>` debería hacer algo cuando alguien hace clic en él.**

  - `<button onkeydown="alert('Hello')">Click me.</button>`
  - `<button onchange ="alert('Hello')">Click me.</button>`
  - `<button onclick ="alert('Hello')">Click me.</button>` ✔

- **El elemento `<div>` debería volverse rojo cuando alguien mueva el mouse sobre él.**

  - `<div onclick="this.style.backgroundColor='red'">myDIV.</div>`
  - `<div onmouseover="this.style.backgroundColor='red'">myDIV.</div>` ✔
  - `<div onmouseclick="this.style.backgroundColor='red'">myDIV.</div>`

- **Utilice el método `getElementsByTagName` para encontrar el primer `<p>` elemento y cambie su texto a `"Hello"`.**

  ```html
  <p id="demo"></p>

  <script>
    ...
  </script>
  ```

  - `document.getElementsByTagName("p")[1].innerHTML= "Hello";`
  - `document.getElementsByTagName("p")[0].innerHTML= "Hello";` ✔
  - `document.getElementsByTagName("p").innerHTML= "Hello";`

- **Utilice HTML DOM para cambiar el valor del atributo `src` de la imagen.**

  ```html
  <img id="image" src="smiley.gif" />

  <script>
    ...
  </script>
  ```

  - `document.getElementById("image").source = "pic_mountain.jpg";`
  - `document.getElementById("image").image= "pic_mountain.jpg";`
  - `document.getElementById("image").src = "pic_mountain.jpg";` ✔

- **Cambie el color del texto del `<p>` elemento a `"rojo"`.**

  ```html
  <p id="demo"></p>

  <script>
    ...
  </script>
  ```

  - `document.getElementById("demo").color = "red";`
  - `document.getElementById("demo").style.color = "red";` ✔
  - `document.getElementById("demo").fontColor = "red";`

- **Agregue la siguiente propiedad y valor al objeto de persona: país: Noruega.**

  ```js
  var person = {
  nombre: "Juan",
  lastName: "Doe",
  ...
  };
  ```

  - `pais: "Noruega",` ✔
  - `pais: "Noruega";`
  - `pais: "Noruega"` ✔

- **Cree un bucle que recorra cada elemento del array frutas**

  ```js
  var frutas = ["Manzana", "Plátano", "Naranja"];

  for (_____) {

    console.log(frutas[i]);

  }
  ```

  - `i of frutas`
  - `i in frutas` ✔
  - `i=0;i<3;i++` ✔

- **Muestre "John" extrayendo información del objeto person.**

  ```js
  var person = {
    firstName: "John",

    lastName: "Doe",
  };

  alert(_____);
  ```

  - `firstname.person`
  - `person.firstName` ✔
  - `firstname`

- **Cree un objeto llamado persona con `nombre = John, edad = 50`. Luego, acceda al objeto para alertar ("John tiene 50").**

  ```js
  var person = {
    name: "John",
    age: 50,
  };
  ```

  - `alert(person.name + " is " +person.age );` ✔
  - `alert(person.name , " is " , person.age );`
  - `alert(name + " is " + age );`

- **¿Cuál es la principal diferencia entre `localStorage` y `sessionStorage`?**

  - localStorage y sessionStorage solo pueden almacenar datos de tipo string
  - No hay diferencia entre localStorage y sessionStorage, ambos almacenan los datos de forma temporal
  - sessionStorage almacena los datos de forma permanente en el navegador, mientras que localStorage almacena los datos solo durante la sesión actual.
  - localStorage almacena los datos de forma permanente en el navegador, mientras que sessionStorage almacena los datos solo durante la sesión actual ✔

---

## **VUE**

- **¿Qué es Vue?**

  - Framework de CSS
  - Framework de HTML
  - Framework de JS ✔

- **Seleccione las afirmaciones que considere verdadera**

  - Vue.js le permite extender HTML con atributos HTML llamados directivas ✔
  - Las directivas Vue.js ofrecen funcionalidad a aplicaciones HTML ✔
  - Vue.js proporciona una función de directrices y directivas definidas por el usuario ✔

- **Vue.js usa como marcadores de posición para los datos:**

  - $ { }
  - { }
  - { { } } ✔

- **Cuando un objeto Vue está vinculado a un elemento HTML, el elemento HTML cambiará cuando el objeto Vue cambie.**

  - Verdadero ✔
  - Falso

- **Marque las directivas que conoce:**

  - v-if-else
  - v-if ✔
  - v-if-elseif

- **La directiva **\_\_** vincula el valor de los elementos HTML a los datos de la aplicación.**

  - v-if
  - v-for
  - v-model ✔

- **Usando la directiva **\_\_** es para vincular un vector de objetos Vue a un vector de elementos HTML**

  - v-model
  - v-for ✔
  - v-if

- **La directiva v-show dispara transiciones cuando su condición cambia**

  - Verdadero ✔
  - Falso

- **Podemos usar la directiva v-on para escuchar eventos DOM y ejecutar algunos JavaScript cuando se activan**

  - Verdadero ✔
  - Falso

- **“Se utiliza mayoritariamente para construir aplicaciones web como dashboards en los que no necesitamos «navegar» a través de una serie de páginas.” ¿A qué concepto corresponde esta definición?**

  - MPA
  - SPA ✔
  - Ninguna de las anteriores
