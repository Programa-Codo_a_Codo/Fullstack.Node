# **Examen Integrador FrontEnd**

- **¿Cuál de las siguientes etiquetas se utiliza para definir una lista numerada en HTML?**

  - `<dl>`
  - `<li>`
  - `<ol>` ✔
  - `<ul>`

- **La definición de una lista en HTML comienza con:**

  - `<list>`
  - `<ul>` ✔
  - `<hr>`
  - `<map>`

- **¿Cuál de las siguientes etiquetas se utiliza para agrupar el contenido del cuerpo de una tabla en HTML?**

  - `<tbody>` ✔
  - `<group>`
  - `<section>`
  - `<container>`

- **¿Cómo relacionamos un código HTML con un archivo de CSS llamado estilos.css?**

  - `<link rel="STYLECASCADE" href="estilos.css">`
  - `<link rel="STYLESHEET" href="estilos.css">` ✔
  - `<link rel="CSS" href="estilos.css">`
  - `<link href="STYLESHEET" rel="estilos.css">`
  - `<link rel="STYLESHEET" css="estilos.css">`
  - `<link rel="estilos.css" href="HTML">`

- **Para un código HTML como `<p class="nota">`, accederíamos a él desde CSS utilizando:**

  - `$nota`
  - `/nota`
  - `#nota`
  - `.nota` ✔
  - `{nota}`

- **¿Cuál de los siguientes valores NO se refiere a un tipo de posicionamiento CSS?**

  - `absolute`
  - `relative`
  - `static`
  - `header` ✔

- **¿Cuál de las siguientes opciones NO es un método de la clase Array en JavaScript?**

  - `forEach()`
  - `reduce()`
  - `extract()` ✔
  - `map()`

- **Dado el siguiente elemento: `<h1 class="titulo" id="tituloPpal">Título principal</h1>` ¿Cuál de las siguientes maneras es incorrecta para acceder al mismo desde un script de JS?**

  - `document.querySelector(".titulo");`
  - `document.getElementByTagName("h1");`
  - `document.querySelector("#tituloPpal");`
  - `document.getElementById("titulo");` ✔

- **Dado el siguiente fragmento de código `let dato = 123 + "1"` Seleccione una:**

  - Obtengo “undefined” porque son distintos tipos de datos.
  - Obtengo como resultado una cadena, porque en la adición JS pondera la concatenación de literales. ✔
  - Obtengo como resultado un número entero, porque en la adición pondera la suma.
  - Lanza una excepción porque intenta operar distintos tipos de datos.
