## **Pasos para correr la app**

1. Dirigirse a la carpeta sql > `initialization_db.sql`, y ejecutar en MySQL Workbench.
   - Ver `DER - Point of sale.png` para entender la estructura de la base de datos.
2. Ejercutar a modo de prueba los endpoints. Para ello importar a postman el archivo postman > `Point-of-sale.postman_collection`.
3. Ejecutar en la linea de comandos
   ```js
   node --watch index.js
   ```
